/**************************************************
** GAME VARIABLES
**************************************************/
var canvas,			// Canvas DOM element
	ctx,			// Canvas rendering context
	drone,			// Local drone
	keys_pressed,	// Pilot's keys pressed on keyboard
	socket;			// Socket connection


/**************************************************
** GAME INITIALISATION
**************************************************/
function init() {
	// Declare the canvas and rendering context
	canvas = document.getElementById("gameCanvas");
	ctx = canvas.getContext("2d");

	// Maximise the canvas
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;

	// Initialise the local drone
	drone = Drone();

	// Initialise socket connection
	socket = io.connect("http://localhost", {port: 8000, transports: ["websocket"]});

	// Start listening for events
	setEventHandlers();
};


/**************************************************
** GAME EVENT HANDLERS
**************************************************/
var setEventHandlers = function() {
	// Keyboard
	window.addEventListener("keydown", onKeydown, false);
	window.addEventListener("keyup", onKeyup, false);

	// Window resize
	window.addEventListener("resize", onResize, false);

	// Socket connection successful
	socket.on("connect", onSocketConnected);

	// Socket disconnection
	socket.on("disconnect", onSocketDisconnect);

};


// Keyboard key down
function onKeydown(e) {
	//console.log('Key down event');
};

// Keyboard key up
function onKeyup(e) {
	//console.log('Key up event');
};

// Browser window resize
function onResize(e) {
	// Maximise the canvas
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;
};

// Socket connected
function onSocketConnected() {
	console.log("Connected to node-ar-drone control server");

};

// Socket disconnected
function onSocketDisconnect() {
	console.log("Warning: disconnected from node-ar-drone control server");
};

// Start Drone Controller loop
var drone_controller = function () {

	function listen() {
		keys_pressed = KeyboardJS.activeKeys();
	}

	function update() {
		if (drone.update_params(keys_pressed)) {
			console.log('Send new drone instructions: ' + drone.status_str());
			console.log(drone.status());
			socket.emit("update drone", { 'status': drone.status() });
		}
	}

	// render pilot drone data
	function render() {
		// console.log('drone_controller is running');
	}

	// main loop
	var main = function () {
		var now = Date.now();
		var delta = now-before;
		listen();
		update();
		render();

		before=now;
	}

	var before = Date.now();
	var loopDroneControllerInterval = setInterval(main, 1); // 1 = Execute as fast as possible; 1000 = execute every second
};


drone_controller(); // start drone controller