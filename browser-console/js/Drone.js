/**************************************************
 ** DRONE CLASS
 **************************************************/
var Drone = function() {
    var stop = false;
    var flying_status = false;
    var pitch_speed = 0;
    var slide = 0;
    var clockwise = 0;
    var vertical_speed = 0;
    var engines_on = false;

    var update_drone_params = function(keys_pressed) {
        // save drone params, we need them later to see if params did change:
        // we send data to drone client only if they did.
        var prev_pitch = pitch_speed;
        var prev_slide = slide;
        var prev_clockwise = clockwise;
        var prev_stop = stop;
        var prev_flying_status = flying_status;
        var prev_vertical_speed = vertical_speed;
        var prev_engines_on = engines_on;

        var is_in_keys_pressed = function(key_str) {
            return keys_pressed.indexOf(key_str.toLowerCase()) > -1;
        };

        if (is_in_keys_pressed('p')) {
            engines_on = true;
            console.log('Drone engines started: taking off...')
        }

        if (is_in_keys_pressed('l')) {
            engines_on = false;
            console.log('Drone landing, starting descent...')
        }

        if (is_in_keys_pressed('spacebar') && engines_on) {
            stop = true;
        } else {
            stop = false;
        }

        // if w pressed go forward, s goes back
        if (is_in_keys_pressed('w')) {
            pitch_speed = 0.2;
        } else if (is_in_keys_pressed('s')) {
            pitch_speed = -0.2;
        } else {
            pitch_speed = 0;
        }

        // if a pressed go left, d goes right
        if (is_in_keys_pressed('a')) {
            slide = -0.2;
        } else if (is_in_keys_pressed('d')) {
            slide = 0.2;
        } else {
            slide = 0;
        }

        // if right pressed turn clockwise, left goes counter clockwise
        if (is_in_keys_pressed('left')) {
            clockwise = -0.7;
        } else if (is_in_keys_pressed('right')) {
            clockwise = 0.7;
        } else {
            clockwise = 0;
        }

        // if up pressed climb, else down
        if (is_in_keys_pressed('up')) {
            vertical_speed = 0.2;
        } else if (is_in_keys_pressed('down')) {
            vertical_speed = -0.2;
        } else {
            vertical_speed = 0;
        }

        var did_params_change = function() {
            return (
                (prev_pitch != pitch_speed) ||
                (prev_slide != slide) ||
                (prev_clockwise != clockwise) ||
                (prev_stop != stop) ||
                (prev_flying_status != flying_status) ||
                (prev_vertical_speed != vertical_speed) ||
                (prev_engines_on != engines_on)
            );
        };

        return did_params_change();
    };

    // Getters and setters
    var is_flying = function() {
      return flying_status
    };

    var switch_flying_status = function() {
        this.flying_status = !this.flying_status;
    };

    var drone_status_str = function() {
        return 'Pitch: '+pitch_speed+'; ';
    }

    var status = function() {
        return {
            flying_status: flying_status,
            pitch: pitch_speed,
            slide: slide,
            clockwise: clockwise,
            vertical_speed: vertical_speed,
            stop: stop,
            engines_on: engines_on
        }
    };

    return {
        flying_status: flying_status,
        is_flying: is_flying,
        switch_flying_status: switch_flying_status,
        update_params: update_drone_params,
        status_str: drone_status_str,
        status: status
    }
};