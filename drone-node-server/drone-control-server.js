/**************************************************
** NODE.JS REQUIREMENTS
**************************************************/
var util = require("util"),					// Utility resources (logging, object inspection, etc)
	io = require("socket.io");				// Socket.IO

/**************************************************
 ** AR DRONE CLIENT
 **************************************************/
var arDrone = require('ar-drone');
var client  = arDrone.createClient();

/**************************************************
** GAME VARIABLES
**************************************************/
var socket,		// Socket controller
	pilots;		// Array of connected pilots

/**************************************************
** GAME INITIALISATION
**************************************************/
function init() {
	// Create an empty array to store pilots
	pilots = [];

	// Set up Socket.IO to listen on port 8000
	socket = io.listen(8000);

	// Configure Socket.IO
	socket.configure(function() {
		// Only use WebSockets
		socket.set("transports", ["websocket"]);
		// Restrict log output
		socket.set("log level", 2);
	});

	// Start listening for events
	setEventHandlers();
};


/**************************************************
** EVENT HANDLERS
**************************************************/
var setEventHandlers = function() {
	// Socket.IO
	socket.sockets.on("connection", onSocketConnection);
};

// New socket connection
function onSocketConnection(client) {
	util.log("Pilot has connected");

	// Listen for client disconnected
	client.on("disconnect", onClientDisconnect);

	// Listen for move player message
	client.on("update drone", onUpdateDrone);
};

// Socket client has disconnected
function onClientDisconnect() {
	util.log("Pilot disconnected");
	util.log('Drone emergency landing!');

	var callback = function() {
		util.log('Drone landed.')
	}

	client.land(callback);
};


// AR Drone actual status
var Erdrone = function(){
	var is_flying = false;

	return {
		is_flying: is_flying
	}
};

var erdrone = Erdrone();

// send new flight commands to AR Drone
function onUpdateDrone(data) {

	// FIRST CONTROL: ALWAYS CHECK IF DRONE SHOULD BE FLYING OR NOT
	// check if command about engines arrived TODO: if callback doesn't arrive block drone commands
	if (data.status.engines_on && !erdrone.is_flying) {
		util.log('Drone taking off...');
		var callback_takeoff = function() {
			util.log('Drone is hovering.')
		};
		client.takeoff(callback_takeoff);
		erdrone.is_flying = true;
	}
	if (!data.status.engines_on && erdrone.is_flying){
		util.log('Drone landing...');
		var callback_land = function() {
			util.log('Drone on ground.')
		};
		client.land(callback_land);
		erdrone.is_flying = false;
	}

	// SECOND CONTROL: ALWAYS CHECK IF DRONE SHOULD BE MOVING OR NOT IF DRONE IS FLYING
	if (data.status.engines_on && erdrone.is_flying) { // drone is flying
		if (data.status.stop) {
			util.log('Drone stop!');
			client.stop();
		} else {
			// Pitch
			if (data.status.pitch >= 0) {
				util.log('AR drone > front ' + data.status.pitch);
				client.front(data.status.pitch);
			} else {
				util.log('AR drone > back ' + (-1 * data.status.pitch));
				client.back(-1 * data.status.pitch);
			}
			// Slide
			if (data.status.slide >= 0) {
				util.log('AR drone > right ' + data.status.slide);
				client.right(data.status.slide);
			} else {
				util.log('AR drone > left ' + (-1 * data.status.slide));
				client.left(-1 * data.status.slide);
			}
			// Turn
			if (data.status.clockwise >= 0) {
				util.log('AR drone > turn right ' + data.status.clockwise);
				client.clockwise(data.status.clockwise);
			} else {
				util.log('AR drone > turn left ' + data.status.clockwise);
				client.clockwise(data.status.clockwise);
			}

			// Climb Up Down
			if (data.status.vertical_speed >= 0) {
				util.log('AR drone > climb up' + data.status.vertical_speed);
				client.up(data.status.vertical_speed);
			} else {
				util.log('AR drone > climb down' + data.status.vertical_speed);
				client.down(-1 * data.status.vertical_speed);
			}
		}
	}
};

/**************************************************
** RUN
**************************************************/
init();